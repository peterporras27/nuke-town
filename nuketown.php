<?php 

$secret_code = '';


/**
Do not alter code after this line unless
you know what you are doing.
**/

$backup_name = '';
if (isset($_POST['code'])) 
{
	// validate code
	if ($_POST['code']==$secret_code) 
	{
		$files = [];

		// remove core files
		if (file_exists(getcwd().'/wp-admin')) {
			nuke('wp-admin');	
		}

		// remove core files
		if (file_exists(getcwd().'/wp-includes')) {
			nuke('wp-includes');
		}

		// get all the remaining files
		if ($handle = opendir('.')) {
		    while (false !== ($entry = readdir($handle))) {
		        if ($entry != "." && $entry != "..") {
		            $files[] = $entry;
		        }
		    }
		    closedir($handle);
		}

		if (isset($_POST['backup_db'])) 
		{
			// get database credentials
			if ( file_exists(getcwd().'/wp-config.php') ) 
			{
				$dbname = getDBinfo('DB_NAME');
				$dbuser = getDBinfo('DB_USER');
				$dbpass = getDBinfo('DB_PASSWORD');
				$dbhost = getDBinfo('DB_HOST');

				$defaults = ['database_name_here','username_here'];
				$notDefault = true;
				if (in_array($dbname, $defaults)) { $notDefault = false;}
				if (in_array($dbuser, $defaults)) { $notDefault = false;}
				
				// Backup Database
				if ($notDefault) {
					exportDatabase($dbhost, $dbname, $dbuser, $dbpass);	
				}
			}

			// Zip wp-content for backup
			if (file_exists(getcwd().'/wp-content')) 
			{
				$backup_name = zipWPcontent();

				// If password is used
				if ( isset($_POST['use_password']) && isset($_POST['password']) ) 
				{
					$pass = fopen('info.txt', "w");
					fwrite($pass, $_POST['password']); // set password
					fclose($pass);
					
					// Register ZIP password
					echo '<!--'.system('zip -P pass '.$backup_name.' info.txt').'-->';

					// Remove password text.
					$files[] = 'info.txt';
				}

				// delete wp-content folder
				nuke('wp-content');
			}
		}

		// delete remaining files
		foreach ($files as $file) 
		{
			if (file_exists(getcwd().'/'.$file)) 
			{
				if ( $file != 'nuketown.php' && $file != $backup_name ) 
				{
					unlink(getcwd().'/'.$file);
				}
			}
		}

		// delete index file if exist
		if (file_exists(getcwd().'/index.php')) {
			unlink(getcwd().'/index.php');
		}

		if (isset($_POST['message']) && !empty($_POST['message'])) 
		{
			$msg = strip_tags($_POST['message']);
			$msg = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1"><title>Nuke Town</title><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"><!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]--></head><body><div class="container" style="padding-top:20%;"><div class="row"><div class="col-md-12"><h2 class="text-center">'.$msg.'</h2></div></div></div></body></html>';

		} else {

			$msg = '<!-- Silence is golden -->';
		}

		// delete index html file if exist
		if (file_exists(getcwd().'/index.html')) {
			unlink(getcwd().'/index.html');
		}

		// create new index file
		$home = fopen('index.php', "w");
		fwrite($home,$msg);
		fclose($home);

	}
}

function getDBinfo($searchfor)
{
	$contents = file_get_contents(getcwd().'/wp-config.php');
	$result = '';
	$pattern = preg_quote($searchfor, '/');
	// finalise the regular expression, matching the whole line
	$pattern = "/^.*$pattern.*\$/m";
	// search, and store all matching occurences in $matches
	if(preg_match_all($pattern, $contents, $matches)){
	   
	   $found = implode("\n", $matches[0]);
	   $found = explode("'", $found);
	   $result = (isset($found[3])) ? $found[3]:'';
	}

	return $result;
}

function nuke($folder)
{
	$dir = getcwd() . DIRECTORY_SEPARATOR . $folder;

	$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

	foreach($files as $file) 
	{
	    if ($file->isDir()){
	        rmdir($file->getRealPath());
	    } else {
	        unlink($file->getRealPath());
	    }
	}

	rmdir($dir);
}

function exportDatabase($host, $database_name, $username, $password)
{
	$conn = mysqli_connect($host, $username, $password, $database_name);

	if ($conn) {
	
		// Get connection object and set the charset
		$conn->set_charset("utf8");

		// Get All Table Names From the Database
		$tables = array();
		$sql = "SHOW TABLES";
		$result = mysqli_query($conn, $sql);

		while ($row = mysqli_fetch_row($result)) {
		    $tables[] = $row[0];
		}

		$sqlScript = "";
		foreach ($tables as $table) {
		    
		    // Prepare SQLscript for creating table structure
		    $query = "SHOW CREATE TABLE $table";
		    $result = mysqli_query($conn, $query);
		    $row = mysqli_fetch_row($result);
		    
		    $sqlScript .= "\n\n" . $row[1] . ";\n\n";
		    
		    
		    $query = "SELECT * FROM $table";
		    $result = mysqli_query($conn, $query);
		    
		    $columnCount = mysqli_num_fields($result);
		    
		    // Prepare SQLscript for dumping data for each table
		    for ($i = 0; $i < $columnCount; $i ++) {
		        while ($row = mysqli_fetch_row($result)) {
		            $sqlScript .= "INSERT INTO $table VALUES(";
		            for ($j = 0; $j < $columnCount; $j ++) {
		                $row[$j] = $row[$j];
		                
		                if (isset($row[$j])) {
		                    $sqlScript .= '"' . $row[$j] . '"';
		                } else {
		                    $sqlScript .= '""';
		                }
		                if ($j < ($columnCount - 1)) {
		                    $sqlScript .= ',';
		                }
		            }
		            $sqlScript .= ");\n";
		        }
		    }
		    
		    $sqlScript .= "\n"; 
		}

		if(!empty($sqlScript))
		{
		    // Save the SQL script to a backup file
		    $backup_file_name = 'wp-content'.DIRECTORY_SEPARATOR.$database_name . '_backup_' . time() . '.sql';
		    $fileHandler = fopen($backup_file_name, 'w+');
		    $number_of_lines = fwrite($fileHandler, $sqlScript);
		    fclose($fileHandler); 

		    // Download the SQL backup file to the browser
		    /*
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
		    header('Content-Transfer-Encoding: binary');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($backup_file_name));
		    ob_clean();
		    flush();
		    readfile($backup_file_name);
		    exec('rm ' . $backup_file_name); 
		    */
		}

	
	}
}

function zipWPcontent()
{
	// Get real path for our folder
	$rootPath = realpath(getcwd().'/wp-content');

	// Initialize archive object
	$zip = new ZipArchive();
	$backup_name = 'nuke-'.time().'.zip';

	$zip->open($backup_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);

	// Create recursive directory iterator
	/** @var SplFileInfo[] $files */
	$files = new RecursiveIteratorIterator(
	    new RecursiveDirectoryIterator($rootPath),
	    RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file)
	{
	    // Skip directories (they would be added automatically)
	    if (!$file->isDir())
	    {
	        // Get real and relative path for current file
	        $filePath = $file->getRealPath();
	        $relativePath = substr($filePath, strlen($rootPath) + 1);

	        // Add current file to archive
	        $zip->addFile($filePath, $relativePath);
	    }
	}

	// Zip archive will be created only after closing object
	$zip->close();

	return $backup_name;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Nuke Town</title>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			.nuke{margin-top: 5%;}
			.btn-lg, .input-lg{border-radius: 30px;}
			.pass, .passcheck{display: none;}
			.btn.focus, .btn:focus, .btn:hover{outline: none;}
			.scale-up-bottom{-webkit-animation:scale-up-bottom 3s cubic-bezier(.39,.575,.565,1.000) infinite alternate;animation:scale-up-bottom 3s cubic-bezier(.39,.575,.565,1.000) infinite alternate}
			@-webkit-keyframes scale-up-bottom{0%{-webkit-transform:scale(.5);transform:scale(.5);-webkit-transform-origin:50% 100%;transform-origin:50% 100%}100%{-webkit-transform:scale(1);transform:scale(1);-webkit-transform-origin:50% 100%;transform-origin:50% 100%}}@keyframes scale-up-bottom{0%{-webkit-transform:scale(.5);transform:scale(.5);-webkit-transform-origin:50% 100%;transform-origin:50% 100%}100%{-webkit-transform:scale(1);transform:scale(1);-webkit-transform-origin:50% 100%;transform-origin:50% 100%}}
			.vibrate-1{-webkit-animation:vibrate-1 .3s linear infinite both;animation:vibrate-1 .3s linear infinite both}
			@-webkit-keyframes vibrate-1{0%{-webkit-transform:translate(0);transform:translate(0)}20%{-webkit-transform:translate(-2px,2px);transform:translate(-2px,2px)}40%{-webkit-transform:translate(-2px,-2px);transform:translate(-2px,-2px)}60%{-webkit-transform:translate(2px,2px);transform:translate(2px,2px)}80%{-webkit-transform:translate(2px,-2px);transform:translate(2px,-2px)}100%{-webkit-transform:translate(0);transform:translate(0)}}@keyframes vibrate-1{0%{-webkit-transform:translate(0);transform:translate(0)}20%{-webkit-transform:translate(-2px,2px);transform:translate(-2px,2px)}40%{-webkit-transform:translate(-2px,-2px);transform:translate(-2px,-2px)}60%{-webkit-transform:translate(2px,2px);transform:translate(2px,2px)}80%{-webkit-transform:translate(2px,-2px);transform:translate(2px,-2px)}100%{-webkit-transform:translate(0);transform:translate(0)}}
		</style>
	</head>
	<body>
		
		<div class="container nuke">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6" align="center">
					<?php if(empty($backup_name)): ?>
					<img class="rocket img-responsive animate__animated animate__fadeIn" width="300" src="//images.vexels.com/media/users/3/150026/isolated/preview/580cca5efa65c7c69ba5cad9e954698d-space-rocket-illustration-by-vexels.png" alt="">
					<form action="" method="POST" role="form">
						<div class="form-group">
							<input type="text" name="code" class="input-lg form-control" id="" placeholder="SECRET CODE">
						</div>
						<div class="form-group">
							<textarea name="message" rows="5" class="input-lg form-control" placeholder="Message..."></textarea>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="checkbox">
									<label>
										<input name="backup_db" type="checkbox" value="1">
										Backup files and database?
									</label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="checkbox passcheck">
									<label>
										<input name="use_password" type="checkbox" value="1" disabled>
										Use password for backup file?
									</label>
								</div>
							</div>
						</div>
						<br>
						<div class="row pass">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" name="password" class="input-lg form-control" id="" placeholder="Backup file password" disabled>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-danger btn-lg btn-block">
							SEND NUKE!
						</button>
					</form>
					<?php else: ?>
						<img class="explosion img-responsive scale-up-bottom" style="width:100%;" src="//www.flaticon.com/svg/static/icons/svg/1534/1534158.svg" alt="">
						<br>
						<a href="<?php echo $backup_name; ?>" class="btn btn-danger btn-lg">
							&nbsp;Download Backup&nbsp;
						</a>
					<?php endif; ?>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script>
			var $=jQuery;
			$(document).ready(function(){

				$('form [type="submit"]').click(function(e) {
					e.preventDefault();

					$(this).prop('disabled',true);

					$('.rocket').removeClass('animate__fadeIn');
					$('.rocket').addClass('vibrate-1');

					var counter = 6;
					var interval = setInterval(function() {
					    counter--;
					    // Display 'counter' wherever you want to display it.
					    if (counter <= 0) {

					     	clearInterval(interval);
					     	$('.rocket').removeClass('vibrate-1');
							$('.rocket').addClass('animate__fadeOutUp');
					      	$('[type="submit"]').html('SEND NUKE!'); 

					        return;

					    }else{

					    	$('[type="submit"]').html(counter);
					    }
					}, 500);

					setTimeout(function(){
						$('form').submit();
					},5000);

				});

				$('[name="code"], [name="message"]').focus(function(){
					if ($('.rocket').hasClass('animate__hinge')) {
						$('.rocket').removeClass('animate__hinge');
						$('.rocket').addClass('animate__fadeIn');
					}
				});

				$('[name="backup_db"]').change(function(){
					if ($(this).prop('checked')) {
						$('.passcheck').fadeIn();
						$('[name="use_password"]').prop('disabled',false);
					} else {
						$('.passcheck').fadeOut();
						$('[name="use_password"]').prop('disabled',true);
						$('.pass').fadeOut();
						$('[name="password"]').prop('disabled',true);
						$('[name="use_password"]').prop('checked',false);
						$('[name="use_password"]').prop('disabled',true);
					}
				});

				$('[name="use_password"]').change(function(){
					if ($(this).prop('checked')) {
						$('.pass').fadeIn();
						$('[name="password"]').prop('disabled',false);
					} else {
						$('.pass').fadeOut();
						$('[name="password"]').prop('disabled',true);
					}
				});
			});
		</script>
	</body>
</html>